import React, {Component} from 'react';
import {View, Image, ImageBackground} from 'react-native';
import {
  SoftLightBlend,
  Emboss,
  Earlybird,
  Invert,
  RadialGradient,
} from 'react-native-image-filter-kit';

export default class ImageFilter extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#eee',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={require('./image.jpg')}
          style={{width: 300}}
          resizeMode="contain"
        />
      </View>
    );
  }
}
