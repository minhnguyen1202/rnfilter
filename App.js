/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {
  ToasterCompat,
  RiseCompat,
  WillowCompat,
  ValenciaCompat,
  ReyesCompat,
  WaldenCompat,
  StinsonCompat,
  SlumberCompat,
  NashvilleCompat,
  MoonCompat,
} from 'react-native-image-filter-kit';

const image = (
  <Image
    // source={require('./image.jpg')}
    source={{
      uri:
        'https://images.unsplash.com/photo-1578165219176-ece04edbd053?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
    }}
    style={{width: 375, height: 375, backgroundColor: '#fdd'}}
    resizeMode="contain"
  />
);

const {width, height} = Dimensions.get('window');

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageItem: null,
    };
  }

  renderImage = () => {
    return (
      <Image
        // source={require('./image.jpg')}
        source={{
          uri:
            'https://images.unsplash.com/photo-1578165219176-ece04edbd053?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
        }}
        style={styles.imageView}
        resizeMode="contain"
      />
    );
  };

  // renderItem = (item, index) => {
  //   return (
  //     <View key={index.toString()} style={{padding: 5}}>
  //       <ToasterCompat image={item} />
  //       <Text style={{marginTop: 5}}>{item.title}</Text>
  //     </View>
  //   );
  // };

  render() {
    const {imageItem} = this.state;
    return (
      <View style={{flex: 1, padding: 20}}>
        <View>
          <Image source={imageItem} />
        </View>
        <ScrollView
          // horizontal
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
          decelerationRate="fast"
          pagingEnabled
          snapToInterval={width - 40}
          snapToAlignment={'center'}
          snapToInterval={1}>
          <View>
            {image}
            <Text style={{marginTop: 5}}>Original</Text>
          </View>
          <View>
            <ToasterCompat image={image} />
            <Text style={{marginTop: 5}}>Toaster</Text>
          </View>
          <View>
            <ValenciaCompat image={image} />
            <Text style={{marginTop: 5}}>Valencia</Text>
          </View>
          <View>
            <RiseCompat image={image} />
            <Text style={{marginTop: 5}}>Rise</Text>
          </View>
          <View>
            <WillowCompat image={image} />
            <Text style={{marginTop: 5}}>Willow</Text>
          </View>
          <View>
            <ReyesCompat image={image} />
            <Text style={{marginTop: 5}}>Reyes</Text>
          </View>
          <View>
            <WaldenCompat image={image} />
            <Text style={{marginTop: 5}}>Walden</Text>
          </View>
          <View>
            <StinsonCompat image={image} />
            <Text style={{marginTop: 5}}>Stinson</Text>
          </View>
          <View>
            <SlumberCompat image={image} />
            <Text style={{marginTop: 5}}>Slumber</Text>
          </View>
          <View>
            <NashvilleCompat image={image} />
            <Text style={{marginTop: 5}}>Nashville</Text>
          </View>
          <View>
            <MoonCompat image={image} />
            <Text style={{marginTop: 5}}>Moon</Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 300,
    height: 300,
    backgroundColor: '#fdd',
  },
  imageView: {
    width: 200,
    height: 200,
    backgroundColor: '#fdd',
  },
});
